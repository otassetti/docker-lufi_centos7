#!/bin/bash

##### Check Env vars and set default var if not found or empty
LUFI_PROXY_USED="${LUFI_PROXY_USED:=0}"
LUFI_MAIL_CONTACT="${LUFI_MAIL_CONTACT:=admin@localhost}"
LUFI_SECRETS="${LUFI_SECRETS:=oiifosdlkl552122134sdf}"
LUFI_THEME="${LUFI_THEME:=default}"
LUFI_URL_LENGHT="${LUFI_URL_LENGHT:=8}"
LUFI_MAX_FILES_SIZE="${LUFI_MAX_FILES_SIZE:=104857600}"
LUFI_UPLOAD_DIR="${LUFI_UPLOAD_DIR:=/opt/LUFI_DATA}"

#### 
lufi_conf_tpl='/opt/lufi/lufi.tpl'
lufi_conf_file='/opt/lufi/lufi.conf'


##### Functions to replace in template

# Function for replace pattern
commonReplacePath(){
    local path="$1"
    local pattern="$2"
    local string="$3"
    if [ -f "${path}" -o -d "${path}" ]; then
        find "${path}" -type f | while read file; do
                sed -i "s#$pattern#$string#g" "$file"
        done
    else
        return "1"
    fi
}

#Replace Var in config file
generate_config_file()
{
        cp -rf "${lufi_conf_tpl}" "${lufi_conf_file}"
        commonReplacePath "${lufi_conf_file}" "%LUFI_PROXY_USED%"  "${LUFI_PROXY_USED}"
		commonReplacePath "${lufi_conf_file}" "%LUFI_MAIL_CONTACT%"  "${LUFI_MAIL_CONTACT}"
		commonReplacePath "${lufi_conf_file}" "%LUFI_SECRETS%"  "${LUFI_SECRETS}"		
		commonReplacePath "${lufi_conf_file}" "%LUFI_THEME%"  "${LUFI_THEME}"
		commonReplacePath "${lufi_conf_file}" "%LUFI_URL_LENGHT%"  "${LUFI_URL_LENGHT}"
		commonReplacePath "${lufi_conf_file}" "%LUFI_MAX_FILES_SIZE%"  "${LUFI_MAX_FILES_SIZE}"
		commonReplacePath "${lufi_conf_file}" "%LUFI_UPLOAD_DIR%"  "${LUFI_UPLOAD_DIR}"
}

generate_config_file