#Docker Container for lufi_centos7
FROM        centos:latest
MAINTAINER  Olivier Tassetti otassetti@gmail.com

RUN yum -y install perl-devel
RUN yum -y install perl-CPAN
RUN yum -y install perl-Digest-MD5
RUN yum -y install perl-Compress-Raw-Zlib
RUN yum -y groupinstall "Development Tools"
RUN curl -L http://cpanmin.us | perl - App::cpanminus
RUN cpanm Carton

RUN cd /opt && git clone https://git.framasoft.org/luc/lufi.git
RUN cd /opt/lufi && carton install

WORKDIR /opt/lufi
RUN curl https://gitlab.com/otassetti/docker-lufi_centos7/raw/master/lufi.tpl > lufi.tpl
RUN curl https://gitlab.com/otassetti/docker-lufi_centos7/raw/master/defineLufiConf.sh > /opt/defineLufiConf.sh
RUN bash /opt/defineLufiConf.sh

EXPOSE 8080

#Launch LUFI
CMD cd /opt/lufi &&  carton exec hypnotoad -f script/lufi